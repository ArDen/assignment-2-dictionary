#!/usr/bin/python3

import subprocess

input = ["apple", "water", "itmo", "someword", "", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"]
output = ["it's a tasty fruit which is really important for your health", "a liquid which is neccessary for every human","place where you will break your mental health", "", "", ""]
error = ["", "", "", "Not found", "Not found", "Buffer overflow"]

for i in range(len(input)):
    p = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(input=input[i].encode())
    str_out = stdout.decode().strip()
    str_err = stderr.decode().strip()
    if str_out == output[i] and str_err == error[i]:
        print("Test "+str(i+1)+" passed")
    else:
        print("Test "+str(i+1)+' failed.\nPrinted:\nstdout: "'+str_out+'", stderr: "'+str_err+'"\nExpected:\nstdout: "'+output[i]+'", stderr: "'+error[i]+'"')