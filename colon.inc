%define head 0       ; pointer on the head node

%macro colon 2
    %%next: dq head         ; reference on the next element
    %define head %%next     ; change reference on the next
    db %1, 0                ; save key
    %2:                     ; create a label for definition string
%endmacro