%include 'lib.inc'

global find_word

section .text

;Принимает два аргумента: указатель на нуль-терминированную строку, указатель на начало словаря.
;Она проходится по всему словарю в поисках подходящего ключа. 
;Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.
find_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi
    .loop:
        mov rdi, r12        
        mov rsi, r13        
        add rsi, 8          ; take the next element
        call string_equals  ; compare with searched element
        test rax, rax       ; if equals, we found appropriate key
        jnz .found
        mov r13, [r13]      ; move to the next element
        test r13, r13       ; check if end of list has been reached
        jz .nothing
        jmp .loop       
    .found:
        mov rax, r13        ; return address of the key
        pop r13
        pop r12
        ret 
    .nothing:
        xor rax, rax        ; return 0
        pop r13
        pop r12
        ret