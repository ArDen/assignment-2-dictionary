ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm

main.o: main.asm colon.inc words.inc

program: main.o lib.o dict.o
	$(LD) -o $@ $^

.PHONY: clean test

test:
	python3 test.py
	
clean:
	rm -rf program *.o 