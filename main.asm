%include 'lib.inc'
%include 'words.inc'
%include 'dict.inc'
%define BUFFER_SIZE 255
%define STD_ERR 2
%define SYS_WRITE 1

section .rodata
nothing_msg: db 'Not found', 0
overflow_msg: db 'Buffer overflow', 0

section .text

global _start

_start:
    sub rsp, BUFFER_SIZE    ; allocate memory in stack
    mov rsi, BUFFER_SIZE    ; max length is 255 bytes
    mov rdi, rsp
    call read_word
    test rax, rax           ; check if we managed to read word
    jz .overflow            ; if rax == 0 -> buffer overflow
    mov rdi, rax            ; input word
    mov rsi, head           ; head of the dict
    mov rdi, rax            ; input word -> rdi
    call find_word
    test rax, rax           ; check if found
    je .nothing             ; not found

.found:
    add rax, 8              ; shifting to the key address
    mov rdi, rax          
    call string_length
    inc rax                 ; + NULL-TERMINATED string len
    add rdi, rax            ; rdi == addres + len + NULL-TERMINATED
    call print_string       ; result
    call print_newline
    add rsp, BUFFER_SIZE
    xor rdi, rdi            ; code of success exit
    jmp exit
    
.nothing:
    mov rdi, nothing_msg
    call print_string_err
    jmp exit

.overflow:
    mov rdi, overflow_msg
    call print_string_err
    jmp exit
