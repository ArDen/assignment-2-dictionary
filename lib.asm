section .text    

%define SYS_EXIT 60
%define SYS_WRITE 1
%define STD_OUT 1
%define STD_ERR 2

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

global print_string_err
global print_substring_fd
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, SYS_EXIT		; exit syscall
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0	; while character != NULL-TERMINATOR
        je .end                                
        inc rax			; counter++                                      
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi			; save rdi before calling function
    call string_length
    pop rdi			; return rdi
    mov rdx, rax		; strlen
    mov  rsi, rdi		; pointer to string to rsi
    mov  rax, SYS_WRITE		; 'write' syscall number
    mov  rdi, STD_OUT		; stdout descriptor
    syscall
    ret

print_string_err:
    push rdi			; save rdi before calling function
    call string_length
    mov rdx, rax		; strlen
    pop rsi			; get back rsi
    mov  rdi, STD_ERR		; stderr descriptor
    jmp print_substring_fd

; rdi - fd ; rsi - str ; rdx - strlen
print_substring_fd:
    mov  rax, SYS_WRITE		; 'write' syscall number
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    mov  rdx, 1			; len of 1 character
    push rdi			; save character to stack
    mov  rsi, rsp		; pointer to character to rsi
    mov  rax, SYS_WRITE		; 'write' syscall number
    mov  rdi, STD_OUT		; stdout descriptor
    syscall
    pop rdi			; clear stack
    ret
    
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`		; new line character
    call print_char	
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
   xor rax, rax
   push rax			; NULL-TERMINATOR for the printing future result
   mov rdx, rdi			; move number to rdx
   xor rcx, rcx			; 0 -> counter
   mov r10, 10			; divider 
    .loop:
	mov rax, rdx		; mov number to rax
	xor rdx, rdx		; 0 -> rdx
	div r10			; rax / 10 -> rax, rax % 10 -> rdx (digit)
	add rdx, '0'		; to ASCII from int number
	dec rsp			; allocating memory in stack for the next number
	mov byte [rsp], dl		; low byte of rdx (digit) to stack (for the future printing)
	mov rdx, rax		; save left number (without last digit) 
	inc rcx			; counter++
	cmp rdx, 0		; check if current number != 0 
	jne .loop 		; while num != 0 we continue division
    mov rdi, rsp		; address of the first digit
    push rcx		        ; save counter before calling function
    call print_string
    pop rcx			; return counter
    add rsp, rcx		; return sp to the start point
    pop rax			; and don't forget about NULL-TERMINATOR
    ret
	
	
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0	
    jnl print_uint	; if >= 0 just print_uint
    push rdi
    mov rdi, '-'
    call print_char	; print '-'
    pop rdi
    neg rdi		; make rdi postive
    jmp print_uint	; print positive number

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax				; counter
    .loop:
	mov dl, byte [rdi + rax]
	cmp byte [rsi + rax], dl		; characters comparison
	jne .no					
	cmp byte [rsi + rax], 0			; check if it is the end of string
	je .yes
	inc rax					; counter++
	jmp .loop
    .yes:
	mov rax, 1
	ret
    .no:
	xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax    ; 0 -> rax ('read' syscall number)
    push rax	    ; push 0 to sp
    mov rsi, rsp    ; address of character (character will be 0 if it is the end of thread)
    mov rdx, 1	    ; len (1 char)
    xor rdi, rdi    ; stdin descriptor
    syscall
    pop rax	    ; clear stack
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12			; saving of callee-saved registers
    push r13
    push r14
    mov r12, rdi		; for using it in reading (address of the beginning of buffer)
    mov r13, rsi		; for using it in reading (size of buffer)
    xor r14, r14		; counter 
    .spaces:
        call read_char
        cmp rax, `\t`  		; compare with white-space character
        je .spaces
        cmp rax, `\ `		; compare with tab character
        je .spaces
        cmp rax, `\n`		; compare with '\n' character
        je .spaces
    .word:
        cmp rax, 0		; compare with NULL-TERMINATOR
        je .yes
        cmp rax, `\t`		; compare with white-space character
        je .yes
        cmp rax, `\ `		; compare with tab character
        je .yes
        cmp rax, `\n`		; compare with '\n' character
        je .yes
        cmp r13, r14		; check if buffer has more elements 
        je .no
        mov byte[r12 + r14], al	; move current character to buffer
        inc r14			; counter++
        call read_char 		; next character
        jmp .word		; loop
    .yes:
        mov byte[r12 + r14], 0	; add NULL-TERMINATOR
        mov rdx, r14		; save counter to rdx
        mov rax, r12		; save address to rax
        pop r14			; clear stack
        pop r13
        pop r12
        ret
    .no:
	    xor rax, rax		; ret 0
	    pop r14			; clear stack
	    pop r13
	    pop r12
	    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
   xor rsi, rsi			; for len
   xor rax, rax			; for number
   mov r10, 10			; multiplier 
    .loop:
	xor rcx, rcx		; for digit
	mov cl, [rdi + rsi]	; read digit using moving pointer
	cmp cl, '0'		; check if ASCII code of character < 0 code (not digit)
	jb .no			
	cmp cl, '9'		; check if ASCII code of character > 9 code (not digit)
	ja .no 			
	sub rcx, '0'		; from ASCI to int number
	mul r10			; rax * 10 -> rax
	add rax, rcx		; rax * 10 + rcx (digit) | new number
	inc rsi			; counter++ 
	jmp .loop 		; while character is digit we continue reading
     .no:
	mov rdx, rsi
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'	
    je .minus		; if < 0 we need to parse '-'
    cmp byte [rdi], '+'	
    jne parse_uint	; if number without sign
    inc rdi		; skip '+'
    call parse_uint	; parse positive number
    inc rdx		; count '+' as 1 length
    ret 
    .minus:
	inc rdi		; move to the next character
    	call parse_uint
    	mov r9, -1 	
    	push rdx		
    	mul r9		; make rax < 0
    	pop rdx
    	inc rdx		; count '-' as 1 length
     	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
	mov cl, byte[rdi + rax]		; move character to low byte of rcx
	inc rax				; counter++
	cmp rax, rdx			; check if buf < len
	jg .no	
	mov byte[rsi + rax - 1], cl	; move character to buffer
	cmp cl, 0			; check if character is NULL-TERMINATED
	je .yes				; success
	jmp .loop
    .no:
	xor rax, rax			; 0 -> rax
    .yes:
	ret